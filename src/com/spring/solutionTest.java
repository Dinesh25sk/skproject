package com.spring;
import org.springframework.beans.factory.BeanFactory;  
import org.springframework.beans.factory.xml.XmlBeanFactory;  
import org.springframework.core.io.ClassPathResource;  
import org.springframework.core.io.Resource;  

public class solutionTest {
	public static class Test {  
		public static void main(String[] args) {  
		    Resource resource=new ClassPathResource("applicationContext.xml");  
		    BeanFactory factory=new XmlBeanFactory(resource);  
		      
		    solution sol=(solution)factory.getBean("solutionbean");  
		    sol.displayInfo();  
		}  

}
}
